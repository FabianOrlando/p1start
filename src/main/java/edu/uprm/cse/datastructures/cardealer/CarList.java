package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

// This class will instantiate a list of cars that will be use for the API

public class CarList {
	private static SortedList<Car> carList = new CircularSortedDoublyLinkedList<>(new CarComparator()); 
	
	private CarList(){
		
	}
	  
	public static SortedList<Car> getInstance(){
		return carList;
		
	}

	public static void resetCars() {
		
		carList = new CircularSortedDoublyLinkedList<>(new CarComparator()); 
	}
}