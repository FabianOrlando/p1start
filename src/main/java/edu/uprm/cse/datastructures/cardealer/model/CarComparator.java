package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

//This car comparator class will compare two cars to determine their sorting order

public class CarComparator implements Comparator<Car> {
	
	public int compare(Car obj1, Car obj2) {


		if(obj1.getCarBrand().compareTo(obj2.getCarBrand()) !=0){ // Will identify if both cars have different brands
			return obj1.getCarBrand().compareTo(obj2.getCarBrand()); //Compare both brands
		}
		
		else if(obj1.getCarModel().compareTo(obj2.getCarModel()) !=0) { // Will identify if both cars have different models
			return obj1.getCarModel().compareTo(obj2.getCarModel()); //They have the same brand, but different model
		}
		
		else if(obj1.getCarModelOption().compareTo(obj2.getCarModelOption()) !=0) { // Will identify if both cars have different options
			return obj1.getCarModelOption().compareTo(obj2.getCarModelOption()); //They have the same brand, same model, but different options
		}
		
		return 0; //At this point, they are the same car
	}			
}
