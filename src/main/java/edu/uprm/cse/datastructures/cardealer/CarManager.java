package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

// This class will instantiate the initial path for the Rest API 

@Path("/cars")
public class CarManager {
	
	//Creates the list
	private final  SortedList<Car> carList = CarList.getInstance();                           

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)

	// This method is for getting all of the cars in the list and add them in an array
	public Car[] getAllCars() {
		Car[] carsArray = new Car[carList.size()];

		for (int i = 0; i < carList.size(); i++) {
			carsArray[i] = carList.get(i);
		}

		return carsArray;
	}


	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)


	// This will identify if the target car is in the list and return it if is in the list
	public Car getCar(@PathParam("id") long id){
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId()==id) {
				return carList.get(i);
			}		
		}

		throw new NotFoundException();

	} 

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)

	// This method is use for adding a car instance into the list
	public Response addCar(Car car){

		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == car.getCarId())
				return Response.status(Response.Status.BAD_REQUEST).build();
		}

		if(carList.add(car)) {
			return Response.status(201).build();
		}

		return Response.status(Response.Status.BAD_REQUEST).build();

	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)

	//This method updates a car instance from the list
	public Response updateCar(@PathParam("id") long id, Car car){


		if(car.getCarId() != id) {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == car.getCarId()) {
				if(carList.remove(carList.get(i)) && carList.add(car))
					return Response.status(Response.Status.OK).build();
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();

	}     

	@DELETE
	@Path("/{id}/delete")

	// This method is for deleting a car instance from the list
	public Response deleteCar(@PathParam("id") long id){


		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == id) {
				if(carList.remove(carList.get(i))) {
					return Response.status(Response.Status.OK).build();
				}
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
