package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * This class creates the circular sorted doubly linked list with a dummy header
 * by implementing the sortedList ADT.
 */


public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	
	private int size;
	private Node<E> header;
	private Comparator<E> comp;

	public CircularSortedDoublyLinkedList() {
		this.size = 0;
		this.header = new Node<E>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.comp = new DefaultComparator();
	}	
	
	//Comparator constructor
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this();
		this.comp = comp;
	}

	@Override
	public Iterator<E> iterator() {
		return new CSDLLIterator();
	}

	@Override
	public boolean add(E obj) { //This method will add the target element
		if(obj.equals(null)) { //Will check if the object is not null, if it is then it is going to return false
			return false;
		}

		
		Node<E> newNode = new Node<E>(obj, null, null);
		
		// If the List is empty, the Node created will be added next to the header automatically
		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			newNode.setNext(this.header);
			newNode.setPrev(this.header);
			this.size++;
			return true;
		}


		//If the List is not empty, then it will iterate over the previous element in the array and compare them
		// with the new element. Once it identify that the new element is smaller than one of the previous elements,
		// it will then add it behind that element.
		Node<E> target = this.header.getNext();
		while(target != this.header) {
			
			
			//Will call the comparator class and compare the new object with all the other elements
			if(comp.compare(obj, target.getElement()) <= 0) { 
				newNode.setNext(target);
				newNode.setPrev(target.getPrev());
				target.setPrev(newNode);
				newNode.getPrev().setNext(newNode);
				this.size++;                                                   
				return true;
			}

			target = target.getNext();
		}
		
		
		//If the new element is bigger than all of the other elements then it will be added at the end of the list
		newNode.setNext(this.header);
		newNode.setPrev(this.header.getPrev());
		this.header.setPrev(newNode);
		newNode.getPrev().setNext(newNode);
		this.size++;
		return true;
	}

	@Override
	public int size() { // This method will return the size of the list
		return this.size;  
	}

	@Override
	public boolean remove(E obj) { //This method will identify if the target element is in the list and then remove it
		if(this.contains(obj)){ 
			return this.remove(this.firstIndex(obj));
		}
		return false;
	}

	@Override
	public boolean remove(int index) { //This method will remove an element from the list, at the given index 

		if ((index < 0) || (index >= this.size)) { //Check if the index given is bigger than the size of the List. If it is, it will send an exception             

			throw new IndexOutOfBoundsException();

		}
		
		
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < index; i++) { //It will iterate over the array until it gets to the position at the given index
			temp = temp.getNext();
		}

		//Remove the element
		Node<E> prevtemp = temp.getPrev();
		Node<E> nexttemp = temp.getNext();
		prevtemp.setNext(nexttemp);
		nexttemp.setPrev(prevtemp);
		temp.setElement(null);
		temp.setNext(null);
		temp.setPrev(null);
		this.size--;

		return true;
	}


	@Override
	public int removeAll(E obj) { //This method removes all the instance of the target element and send how many elements where removed

		int count = 0;
		while (this.contains(obj)) { //Iterate over the list to see all the instances of the target element and remove it
			this.remove(obj);
			count++;
		}
		return count;
	}

	@Override
	public E first() { //The first element of the list
		return this.header.getNext().getElement();
	}

	@Override
	public E last() { //The last element of the list
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) { //This method will send the element at the given index
		
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < index; i++) { //The loop will iterate over the list until it gets to the position of the given index.
			temp = temp.getNext();
		}

		return temp.getElement(); //Send the element at that position
	}

	@Override
	public void clear() { //This method will clear all the list. Make it empty.

		Node<E> temp = header.getNext();
		
		while(temp != header) { //This loop will keep going until the list is empty. It will constantly remove the elements.
			
			Node<E> target = temp;
			temp = temp.getNext();
			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
		}

		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.size = 0;
	}

	@Override
	public boolean contains(E e) { //This method will identify if the target element is in the list
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() { //This method will identify if the list is empty
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) { //This method will send the index of the first instance of the target element
		int index = 0;

		Node<E> temp = this.header.getNext();
		while(temp != this.header) { //
			if(temp.getElement().equals(e)) {
				return index;
			}
			temp = temp.getNext();
			index++;
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) { //This method will send the index of the last instance of the target element 
		int index = this.size-1;

		Node<E> temp = this.header.getPrev();
		while(temp != this.header) {
			if(temp.getElement().equals(e)){
				return index;
			}
			temp = temp.getPrev();
			index--;
		}

		return -1;
	}
	
	public void setComparator(Comparator<E> comp) { //Comparator setter 
		this.comp = comp;
	}

	//Node Interface
	private static class Node<E>{
		private Node<E> prev;
		private Node<E> next;
		private E elem;

		public Node(E e, Node<E> prev, Node<E> next){
			this.elem = e;
			this.prev = prev;
			this.next = next;
		}

		public Node() {
			this(null, null, null);
		}

		public Node<E> getPrev(){
			return this.prev;
		}

		public Node<E> getNext(){
			return this.next;
		}

		public E getElement() {
			return this.elem;
		}

		public void setPrev(Node<E> node){
			this.prev = node;
		}

		public void setNext(Node<E> node){
			this.next = node;
		}

		public void setElement(E e) {
			this.elem = e;
		}
	}

	private class DefaultComparator implements Comparator<E>{

		@Override
		public int compare(E o1, E o2) {
			String obj1 = o1.toString();
			String obj2 = o2.toString();
			return obj1.compareTo(obj2);
		}

	}

	private class CSDLLIterator implements Iterator<E>{
		private Node<E> currentNode;

		public CSDLLIterator() {
			this.currentNode = header.getNext();
		}
		@Override
		public boolean hasNext() {
			return this.currentNode != header;
		}

		@Override
		public E next() {
			if(!this.hasNext()) {
				throw new NoSuchElementException();
			}

			E result = currentNode.getElement();
			currentNode = currentNode.getNext();
			return result;
		}

	}
}